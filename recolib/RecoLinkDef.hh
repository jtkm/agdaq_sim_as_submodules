#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class  TFitVertex+;
#pragma link C++ class  TFitHelix+;
#pragma link C++ class  TFitLine+;
#pragma link C++ class  TTrack+;
#pragma link C++ class  TSpacePoint+;

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
