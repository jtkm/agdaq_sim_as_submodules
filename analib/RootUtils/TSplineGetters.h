#include "RootUtils.h"

#ifndef _TSplineGetters_
#define _TSplineGetters_

#include "TSpline.h"

TSpline5* InterpolateVoltageRamp(const char* filename);


#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
