#include "RootUtils.h"
#include "Rtypes.h"

#ifndef _BoolGetters_
#define _BoolGetters_
Bool_t ChronoboxesHaveChannel(Int_t runNumber, const char* Name);

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
